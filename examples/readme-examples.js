
var MyBaseClass = Kone(function(def)
{
    // this is the constructor, it's a good practice to alias the name as self
    var self  = function MyBaseClass(options)
    {
        console.log("creating MyBaseClass instance");
    }
    
    // instance definition last, constructor next to last
    def( self, 
    {
        hello : function()
        {
            console.log("hello base");
        },
        
        color : function()
        {
            console.log("i'm blue");
        }
    });
    
    // def() returns itself so you can chain if you want
    // static definition last, constructor next to last
    def.static( self,
    {
        something : function() {},
        somethingElse : function() {},
    });
    
    return self;
});

// Now let's extend our base class 

// Parents come first in order, each argument that is a parent is passed in
//  order to the definition environment as constructor(function) first, and prototype after;
//  any non function arguments in between are passed as they are
//  (you can use as meny parents as you'd like)
//
//      -- Parent is the constructor, 
//      -- parent is the Parent.prototype
//
var MyClass = Kone( MyBaseClass, function(def, Parent, parent) 
{
    var self  = function MyClass(options)
    {
        Parent.apply(this, arguments);
        console.log("creating MyClass instance");
    }
    
    // instance definition last, constructor next to last,
    //  other prototypes first, merged from left to right
    def( parent, self, 
    {
        color : "yellow",
        
        hello : function()
        {
            parent.hello.call(this);
            console.log("hello my");
        },
        
        myColor : function()
        {
            console.log("my color is " + this.color);
        }
    });
    
    // add static properties too -- always pass an object at the end
    def.static( Parent, self, {});
    
    return self;
});

// Extending further

var MyOtherClass = Kone( MyClass, function(def, Parent, parent) 
{
    var self  = function MyOtherClass(options)
    {
        Parent.apply(this, arguments);
        console.log("creating MyOtherClass instance");
    }
    
    // instance definition last, constructor next to last,
    //  other prototypes first (will be  merged left to right)
    def( parent, self, 
    {
        color : "green",
        
        hello : function()
        {
            parent.hello.call(this);
            console.log("hello other");
        }
        
    }).static( Parent, self, {});
    
    return self;
});

// 
//
//

var base = new MyBaseClass; 
// -> creating MyBaseClass instance

var my = new MyClass; 
// --> creating MyBaseClass instance
// --> creating MyClass instance

var other = new MyOtherClass;
// --> creating MyBaseClass instance
// --> creating MyClass instance
// --> creating MyOtherClass instance

other.hello();
// --> hello base
// --> hello my
// --> hello other

other.myColor()
// --> my color is green


// modify a single instance
//  arguments passed are the constructor, the prototype, 
//  and the awareness, _aware property wich is an array of all the merged
//  prototypes, in reversed order (constructor has an _aware too)
//
Kone.patch(other, function(cons, proto, parents) // --> the awareness
{
    // modify the instance yourself
    this.myColor = function()
    {
        // get base class method
        parents[1].color.call(this);
        console.log("or maybe purple ... surely not "+proto.color+" ... or ... ");
        this.color = parents[0].color;
        proto.myColor.call(this);
    }
    
});

other.myColor();
// --> i'm blue
// --> or maybe purple ... surely not green ... or ...
// --> my color is yellow
