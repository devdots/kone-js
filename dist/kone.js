(function(root, factory)
{
    factory.meta = {root: root, context:this};
    
    if (typeof define == 'function' && define.amd)
    {
        factory.meta.env = 'amd';
        // set up your loader beforehand and set up the paths
        define(factory);
    }
    else if (typeof exports == 'object') 
    {
        factory.meta.env = 'commonjs';
        // -- use an exported require if available
        var req = root.original_require || require; 
        // -- we need this at least for node js, as you cannot change the
        // behavior of require in child modules from a parent module
        module.exports = factory();
    }
    else
    {
        factory.meta.env = 'globals';
        // export globally using global aliases
        root.Kone = factory();
    }
})(typeof global == "object" ? global : this, 
// definition
function()
{
    var factory = arguments.callee;
    
    function Kone()
    {
        var args = [].slice.call(arguments), env = args.pop(), vars = [];
        
        if (typeof env != "function")
            throw new Error("Last Kone argument should always be a function definiton");
        
        vars.push(arguments.callee.def);
        
        for(var i=0; i < args.length; i++)
        {
            if (typeof args[i] == "function")
            {
                vars.push(args[i]);
                vars.push(args[i].prototype);
            }
            else
                vars.push(args[i]);
        }
        
        var cons = env.apply(this, vars);
        
        if (typeof cons == "function" && cons.prototype.constructor !== cons)
        {
            try
            {
                Object.defineProperty(cons.prototype, "constructor", {value:cons});
            }
            catch(e){}
        }
        
        return cons;
    };
    
    
    Kone.def = function()
    {
        var args = [].slice.call(arguments), def = args.pop();
        
        if ( ! def || typeof def != "object")
            throw new Error("Last def argument should always be an abject");
        
        var cons = args.pop(), subject = cons.prototype, awareness = [];
        
        if (typeof cons != "function")
            throw new Error("Next to last def argument should be the constructor");
        
        
        for(var i=0; i < args.length; i++)
        {
            if (args[i] && typeof args[i].hasOwnProperty == "function")
            {
                for(var prop in args[i])
                    if (args[i].hasOwnProperty(prop) && prop != "_aware")
                        subject[prop] = args[i][prop];
                
                if ("_aware" in args[i] && Array.isArray(args[i]._aware))
                    awareness = awareness.concat(args[i]._aware);
            }
        }
        
        for(var prop in def)
            if (def.hasOwnProperty(prop) && prop != "_aware" && prop != "prototype")
                subject[prop] = def[prop];
        
        awareness = awareness.concat(args).reverse();
        
        if ( ! "_aware" in subject)
            Object.defineProperty(subject, "_aware", {value:awareness, configurable:true});
        else if (Array.isArray(subject._aware))
            awareness.forEach(function(thing){ subject._aware.unshift(thing); });
        else if (delete subject._aware)
            Object.defineProperty(subject, "_aware", {value:awareness, configurable:true});
        else
            throw new Error("Awareness meta prop _aware is not configurable and not an array");
            
        Object.defineProperty(subject, "constructor", {value:cons});
        
        return arguments.callee;
    };
    
    
    Kone.patch = function()
    {
        var args = [].slice.call(arguments), env = args.pop(), vars = [];
        
        if (typeof env != "function")
            throw new Error("Last patch argument should always be a function definition");
        
        if (args.length < 1)
            throw new Error("Patch what ? You supplied no instance.");
        
        var subject = args.shift();
        
        if ( ! subject || typeof subject.constructor != "function")
            throw new Error("Cannot patch this object. No constructor");
        
        vars.push(subject.constructor);
        vars.push(subject.constructor.prototype);
        vars.push(Array.isArray(subject._aware) ? subject._aware : []);
        
        env.apply(subject, vars.concat(args) );
        
        return subject;
    };
    
    
    Kone.static = function()
    {
        var args = [].slice.call(arguments), def = args.pop();
        
        if ( ! def || typeof def != "object")
            throw new Error("Last static argument should always be an abject");
        
        var subject = args.pop(), awareness = [];
        
        if (typeof subject != "function")
            throw new Error("Next static last def argument should be the constructor");
        
        for(var i=0; i < args.length; i++)
        {
            if (args[i] && typeof args[i].hasOwnProperty == "function")
            {
                for(var prop in args[i])
                    if (args[i].hasOwnProperty(prop) && prop != "_aware" && prop != "prototype")
                        subject[prop] = args[i][prop];
                
                if ("_aware" in args[i] && Array.isArray(args[i]._aware))
                    awareness = awareness.concat(args[i]._aware);
            }
        }
        
        for(var prop in def)
            if (def.hasOwnProperty(prop) && prop != "_aware" && prop != "prototype")
                subject[prop] = def[prop];
        
        awareness = awareness.concat(args).reverse();
        
        if ( ! "_aware" in subject)
            Object.defineProperty(subject, "_aware", {value:awareness, configurable:true});
        else if (Array.isArray(subject._aware))
            awareness.forEach(function(thing){ subject._aware.unshift(thing); });
        else if (delete subject._aware)
            Object.defineProperty(subject, "_aware", {value:awareness, configurable:true});
        else
            throw new Error("Awareness meta prop _aware is not configurable and not an array");
                        
        return arguments.callee.def;
    }
    
    Kone.def.patch = Kone.patch;
    Kone.def.static = Kone.static;
    Kone.def.static.def = Kone.def;
    
    return Kone;
});
